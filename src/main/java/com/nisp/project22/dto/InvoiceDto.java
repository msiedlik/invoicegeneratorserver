package com.nisp.project22.dto;

import java.io.Serializable;
import java.util.ArrayList;

public class InvoiceDto implements Serializable {
    private String sellerCompany;
    private String sellerName;
    private String sellerAddress;
    private String sellerPostcode;
    private String sellerCity;
    private String buyerName;
    private String buyerAddress;
    private String buyerPostcode;
    private String buyerCity;
    private int taxRate;
    private ArrayList<ProductDto> productDtos;


    public String getSellerCompany() { return sellerCompany; }

    public String getSellerName() { return sellerName; }

    public String getSellerAddress() { return sellerAddress; }

    public String getSellerPostcode() { return sellerPostcode; }

    public String getSellerCity() { return sellerCity; }

    public String getBuyerName() { return buyerName; }

    public String getBuyerAddress() { return buyerAddress; }

    public String getBuyerPostcode() { return buyerPostcode; }

    public String getBuyerCity() { return buyerCity; }

    public int getTaxRate() { return taxRate; }

    public ArrayList<ProductDto> getProductDtos() { return productDtos; }
}
