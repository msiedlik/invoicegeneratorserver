package com.nisp.project22.dto;

import java.io.Serializable;

public class ProductDto implements Serializable {

    private String description;
    private int quantity;
    private double netPrice;


    public String getDescription() { return description; }

    public int getQuantity() { return quantity; }

    public double getNetPrice() { return netPrice; }
}
