package com.nisp.project22.rest;

import com.nisp.project22.dto.InvoiceDto;
import com.nisp.project22.service.DocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InvoiceApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceApiController.class);

    @Autowired
    private DocumentService documentService;

    @PostMapping(value = "/api/invoice", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> createInvoice(@RequestBody InvoiceDto invoiceDto){

        LOGGER.info("#### call of createInvoice function ####");
        byte[] content = documentService.createDocument(invoiceDto);

        return content != null ?
                new ResponseEntity<>(content, HttpStatus.CREATED) :
                new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
