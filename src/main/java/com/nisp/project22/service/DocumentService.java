package com.nisp.project22.service;

import com.nisp.project22.dto.InvoiceDto;

public interface DocumentService {

    byte[] createDocument(InvoiceDto invoiceDto);
}
