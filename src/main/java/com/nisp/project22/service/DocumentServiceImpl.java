package com.nisp.project22.service;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.nisp.project22.dto.InvoiceDto;
import com.nisp.project22.dto.ProductDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.allegro.finance.tradukisto.ValueConverters;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

@Service
public class DocumentServiceImpl implements DocumentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentServiceImpl.class);
    private final String FONT = "/fonts/OpenSans-Regular.ttf";
    private final String FONT_BOLD = "/fonts/OpenSans-Bold.ttf";

    private Font fontBigBold = FontFactory.getFont(FONT_BOLD, BaseFont.IDENTITY_H, 14);
    private Font font = FontFactory.getFont(FONT, BaseFont.IDENTITY_H, 11);
    private Font fontBold = FontFactory.getFont(FONT_BOLD, BaseFont.IDENTITY_H, 11);
    private Font fontBoldWhite = FontFactory.getFont(FONT_BOLD, BaseFont.IDENTITY_H, 11, 0, BaseColor.WHITE);

    @Override
    public byte[] createDocument(InvoiceDto invoiceDto) {

        final ArrayList<ProductDto> productDtos = invoiceDto.getProductDtos();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        try {
            Document document = new Document(PageSize.A4);
            PdfWriter.getInstance(document, byteArrayOutputStream);
            document.open();

            //HEADER AND SPACING
            String currentDate = DateTimeFormatter.ofPattern("d/M/yyyy").format(ZonedDateTime.now());
            Paragraph p = new Paragraph("Faktura nr: " + currentDate, fontBigBold);
            p.setAlignment(Element.ALIGN_CENTER);
            document.add(p);
            Paragraph sp = new Paragraph("\n", fontBigBold);
            sp.setAlignment(Element.ALIGN_CENTER);
            document.add(sp);
            document.add(sp);

            //ADDRESSEES
            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100);
            table.setWidths(new int[]{6, 4});

            PdfPCell seller = insertPartyAddress("Sprzedawca:", invoiceDto.getSellerCompany(), invoiceDto.getSellerName(),
                    invoiceDto.getSellerAddress(), invoiceDto.getSellerPostcode(), invoiceDto.getSellerCity());
            table.addCell(seller);

            PdfPCell buyer = insertPartyAddress("Nabywca:", "\n", invoiceDto.getBuyerName(),
                    invoiceDto.getBuyerAddress(), invoiceDto.getBuyerPostcode(), invoiceDto.getBuyerCity());
            table.addCell(buyer);

            document.add(table);
            document.add(sp);

            DecimalFormat df = new DecimalFormat("0.00");

            //ITEMS TABLE
            PdfPTable itemTable = new PdfPTable(4);
            itemTable.setWidthPercentage(100);
            itemTable.setSpacingBefore(10);
            itemTable.setSpacingAfter(10);
            itemTable.setWidths(new int[]{7, 3, 3, 3});

            //HEADER
            itemTable.addCell(insertTitleCell("Opis"));
            itemTable.addCell(insertTitleCell("Liczba"));
            itemTable.addCell(insertTitleCell("Cena jedn.(zł)"));
            itemTable.addCell(insertTitleCell("Koszt (zł)"));

            //PRODUCTS
            double netCost = 0;
            for (ProductDto productDto : productDtos) {
                itemTable.addCell(insertCell(productDto.getDescription(), Element.ALIGN_LEFT, 1.3f, 0.1f, 0.1f));
                itemTable.addCell(insertCell(String.valueOf(productDto.getQuantity()), Element.ALIGN_RIGHT, 0.1f, 0.1f, 0.1f));
                itemTable.addCell(insertCell(String.valueOf(df.format(productDto.getNetPrice())), Element.ALIGN_RIGHT, 0.1f, 0.1f, 0.1f));
                itemTable.addCell(insertCell(String.valueOf(df.format(productDto.getNetPrice() * productDto.getQuantity())), Element.ALIGN_RIGHT, 0.1f, 1.3f, 0.1f));
                netCost += productDto.getNetPrice() * productDto.getQuantity();
            }

            //NETTO
            itemTable.addCell(insertCell("", Element.ALIGN_LEFT, 1.3f, 0.1f, 0.1f)).setBorderWidthTop(1.3f);
            itemTable.addCell(insertCell("", Element.ALIGN_LEFT, 0.1f, 0.1f, 0.1f)).setBorderWidthTop(1.3f);
            itemTable.addCell(insertCell("Netto", Element.ALIGN_LEFT, 0.1f, 0.1f, 0.1f)).setBorderWidthTop(1.3f);
            itemTable.addCell(insertCell(String.valueOf(df.format(netCost)), Element.ALIGN_RIGHT, 0.1f, 1.3f, 0.1f)).setBorderWidthTop(1.3f);

            //TAX
            itemTable.addCell(insertCell("", Element.ALIGN_LEFT, 1.3f, 0.1f, 0.1f));
            itemTable.addCell(insertCell("Podatek", Element.ALIGN_LEFT, 0.1f, 0.1f, 0.1f));
            itemTable.addCell(insertCell(invoiceDto.getTaxRate() + " %", Element.ALIGN_RIGHT, 0.1f, 0.1f, 0.1f));
            double taxCost = netCost * (invoiceDto.getTaxRate() / 100.0);
            itemTable.addCell(insertCell(String.valueOf(df.format(taxCost)), Element.ALIGN_RIGHT, 0.1f, 1.3f, 0.1f));

            //TABLE SUMMARY
            itemTable.addCell(insertCell("", Element.ALIGN_LEFT, 1.3f, 0.1f, 1.6f)).setBorderWidthTop(1.3f);
            itemTable.addCell(insertCell("", Element.ALIGN_LEFT, 0.1f, 0.1f, 1.6f)).setBorderWidthTop(1.3f);
            itemTable.addCell(insertCell("Łącznie", Element.ALIGN_LEFT, 0.1f, 0.1f, 1.6f)).setBorderWidthTop(1.3f);
            double summaryPrice = Math.floor(((netCost) + taxCost) * 100) / 100;
            itemTable.addCell(insertCell(String.valueOf(df.format(summaryPrice)), Element.ALIGN_RIGHT, 0.1f, 1.3f, 1.6f)).setBorderWidthTop(1.3f);

            document.add(itemTable);
            document.add(sp);

            //SUMMARY AND WORDS REPRESENTATION
            Paragraph pp = new Paragraph("Do zapłaty: ", fontBold);
            pp.add(new Chunk(df.format(summaryPrice) + " zł", font));
            document.add(pp);
            Paragraph ppAsWords = new Paragraph("Słownie: ", fontBold);
            ppAsWords.add(new Chunk(getSumAsWords(summaryPrice), font));
            document.add(ppAsWords);

            document.close();
            return byteArrayOutputStream.toByteArray();

        } catch (DocumentException e) {
            LOGGER.info("i can't create document or file not exists");
        }
        return null;
    }

    private PdfPCell insertPartyAddress(String who, String name, String line1, String line2, String postcode, String city) {
        String postcodeAndCity = postcode + " " + city;
        PdfPCell cell = new PdfPCell();
        cell.setBorder(PdfPCell.NO_BORDER);
        cell.addElement(new Paragraph(who, fontBold));
        cell.addElement(new Paragraph(name, font));
        cell.addElement(new Paragraph(line1, font));
        cell.addElement(new Paragraph(line2, font));
        cell.addElement(new Paragraph(postcodeAndCity, font));

        return cell;
    }

    private PdfPCell insertTitleCell(String value) {
        PdfPCell cell = new PdfPCell();
        cell.setUseAscender(true);
        cell.setUseDescender(true);
        cell.setPadding(6);
        cell.setPaddingBottom(15);
        cell.setBackgroundColor(BaseColor.DARK_GRAY);
        cell.setBorder(Rectangle.NO_BORDER);
        Paragraph p = new Paragraph(value, fontBoldWhite);
        p.setAlignment(Element.ALIGN_LEFT);
        cell.addElement(p);

        return cell;
    }

    private PdfPCell insertCell(String value, int align, float lBorder, float rBorder, float bBorder) {
        PdfPCell cell = new PdfPCell();
        cell.setUseAscender(true);
        cell.setUseDescender(true);
        cell.setPadding(6);
        cell.setBorderColor(BaseColor.DARK_GRAY);
        cell.setBorderWidthLeft(lBorder);
        cell.setBorderWidthRight(rBorder);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(bBorder);
        Paragraph p = new Paragraph(value, font);
        p.setAlignment(align);
        cell.addElement(p);

        return cell;
    }

    private String getSumAsWords(double summaryPrice){

        String[] strParts = String.valueOf(summaryPrice).split("\\.");
        String plnStr = strParts[0];
        String grStr = strParts[1];
        ValueConverters converter = ValueConverters.POLISH_INTEGER;
        String plnAsWords = converter.asWords(Integer.parseInt(plnStr));
        String grAsWords = converter.asWords(Integer.parseInt(grStr));
        String sumAsWords = plnAsWords + " ";

        if (Integer.parseInt(plnStr) == 1) sumAsWords += "złoty";
        else if (isPluralDenominator(plnStr)) sumAsWords += "złote";
        else sumAsWords += "złotych";

        sumAsWords += " i " + grAsWords + " ";

        if (Integer.parseInt(grStr) == 1) sumAsWords += "grosz";
        else if (isPluralDenominator(grStr)) sumAsWords += "grosze";
        else sumAsWords += "groszy";

        return sumAsWords + ".";
    }

    private boolean isPluralDenominator(String str){
        return (((str.charAt(str.length() - 1) == '2') || (str.charAt(str.length() - 1) == '3')
                || (str.charAt(str.length() - 1) == '4')) && (str.charAt(str.length() - 2) != '1'));
    }
}
